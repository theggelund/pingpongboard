package no.dips.pingpongscore.backend.dto;

public class Result {

    private String playerName;

    private int wins;

    private int losses;

    private int totalPoints;

    private int totalNegativePoints;

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getLosses() {
        return losses;
    }

    public void setLosses(int losses) {
        this.losses = losses;
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }

    public int getTotalNegativePoints() {
        return totalNegativePoints;
    }

    public void setTotalNegativePoints(int totalNegativePoints) {
        this.totalNegativePoints = totalNegativePoints;
    }

    public void incWins() {
        this.wins++;
    }

    public void incLosses() {
        this.losses++;
    }
}
