package no.dips.pingpongscore.backend.dto;

import java.util.Date;
import java.util.UUID;

public class Score {

    private UUID id;

    private String playerOne;

    private String playerTwo;

    private int scorePlayerOne;

    private int scorePlayerTwo;

    private Date playTime;

    public Score(UUID id, String playerOne, String playerTwo, int scorePlayerOne, int scorePlayerTwo, Date playTime) {
        this.id = id;
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
        this.scorePlayerOne = scorePlayerOne;
        this.scorePlayerTwo = scorePlayerTwo;
        this.playTime = playTime;
    }

    public Score() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getPlayerOne() {
        return playerOne;
    }

    public void setPlayerOne(String playerOne) {
        this.playerOne = playerOne;
    }

    public String getPlayerTwo() {
        return playerTwo;
    }

    public void setPlayerTwo(String playerTwo) {
        this.playerTwo = playerTwo;
    }

    public int getScorePlayerOne() {
        return scorePlayerOne;
    }

    public void setScorePlayerOne(int scorePlayerOne) {
        this.scorePlayerOne = scorePlayerOne;
    }

    public int getScorePlayerTwo() {
        return scorePlayerTwo;
    }

    public void setScorePlayerTwo(int scorePlayerTwo) {
        this.scorePlayerTwo = scorePlayerTwo;
    }

    public Date getPlayTime() {
        return playTime;
    }

    public void setPlayTime(Date playTime) {
        this.playTime = playTime;
    }
}
