package no.dips.pingpongscore.backend.mapping;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
public class PlayResult extends MappingBase {

    @ManyToOne(optional = false)
    @Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
    private Player playerOne;

    @ManyToOne(optional = false)
    @Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
    private Player playerTwo;

    @Column(nullable = false)
    private int scorePlayerOne;

    @Column(nullable = false)
    private int scorePlayerTwo;

    @Column(nullable = false)
    private Date playTime;

    public Player getPlayerOne() {
        return playerOne;
    }

    public void setPlayerOne(Player playerOne) {
        this.playerOne = playerOne;
    }

    public Player getPlayerTwo() {
        return playerTwo;
    }

    public void setPlayerTwo(Player playerTwo) {
        this.playerTwo = playerTwo;
    }

    public Date getPlayTime() {
        return playTime;
    }

    public void setPlayTime(Date playTime) {
        this.playTime = playTime;
    }

    public int getScorePlayerOne() {
        return scorePlayerOne;
    }

    public void setScorePlayerOne(int scorePlayerOne) {
        this.scorePlayerOne = scorePlayerOne;
    }

    public int getScorePlayerTwo() {
        return scorePlayerTwo;
    }

    public void setScorePlayerTwo(int scorePlayerTwo) {
        this.scorePlayerTwo = scorePlayerTwo;
    }
}
