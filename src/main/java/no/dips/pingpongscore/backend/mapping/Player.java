package no.dips.pingpongscore.backend.mapping;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.util.Set;

@Entity
public class Player extends MappingBase {

    @Column(nullable = false, unique = true)
    private String name;

    @OneToMany
    @JoinColumn
    @Cascade(CascadeType.ALL)
    private Set<PlayResult> playResults;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
