package no.dips.pingpongscore.backend.repository;

public class MissingRequiredFieldException extends Exception {

    public MissingRequiredFieldException(String message, Throwable cause) {
        super(message, cause);
    }

    public MissingRequiredFieldException(String message) {
        super(message);
    }
}
