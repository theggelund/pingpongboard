package no.dips.pingpongscore.backend.repository;

import no.dips.pingpongscore.backend.mapping.Player;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

//@org.springframework.stereotype.Repository
public class PlayerRepository extends Repository<Player> {

    @Transactional
    public Player getByName(String name) {
        return (Player) getSession().createCriteria(Player.class)
                .add(Restrictions.eq("name", name))
                .uniqueResult();
    }
}
