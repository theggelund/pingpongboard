package no.dips.pingpongscore.backend.repository;

import no.dips.pingpongscore.backend.dto.Score;
import no.dips.pingpongscore.backend.mapping.PlayResult;
import no.dips.pingpongscore.backend.mapping.Player;
import org.apache.cxf.helpers.CastUtils;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@org.springframework.stereotype.Repository
public class ScoreRepository {

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private Repository<PlayResult> playResultRepository;

    @Transactional
    public void add(Score score) throws MissingRequiredFieldException {

        if (score.getPlayerOne() == null) {
            throw new MissingRequiredFieldException("playerOne");
        }
        if (score.getPlayerTwo() == null) {
            throw new MissingRequiredFieldException("playerTwo");
        }

        Player playerOne = getPlayer(score.getPlayerOne());
        Player playerTwo = getPlayer(score.getPlayerTwo());

        PlayResult result = new PlayResult();
        result.setPlayerOne(playerOne);
        result.setPlayerTwo(playerTwo);

        result.setScorePlayerOne(score.getScorePlayerOne());
        result.setScorePlayerTwo(score.getScorePlayerTwo());
        result.setPlayTime(score.getPlayTime());

        playResultRepository.saveOrUpdate(result);

        score.setId(result.getId());
    }

    private Player getPlayer(String playerName) throws MissingRequiredFieldException {
        Player player = playerRepository.getByName(playerName);
        if (player == null) {
            player = new Player();
            player.setName(playerName);
        }

        return player;
    }

    @Transactional(readOnly = true)
    public List<Score> getOrderByPlayTime(int maxCount) {
        List<PlayResult> results = CastUtils.cast(playResultRepository.getSession()
                .createCriteria(PlayResult.class)
                .setMaxResults(maxCount)
                .setFetchMode("playerOne", FetchMode.JOIN)
                .setFetchMode("playerTwo", FetchMode.JOIN)
                .addOrder(Order.desc("playTime"))
                .list());

        return toScore(results);
    }

    @Transactional(readOnly = true)
    public List<Score> getLastThirtyDaysScore() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MONTH, -1);

        Date greaterThan = calendar.getTime();

        List<PlayResult> results = CastUtils.cast(playResultRepository.getSession()
                .createCriteria(PlayResult.class)
                .setFetchMode("playerOne", FetchMode.JOIN)
                .setFetchMode("playerTwo", FetchMode.JOIN)
                .add(Restrictions.gt("playTime", greaterThan))
                .list());

        return toScore(results);
    }

    @Transactional(readOnly = true)
    public Score get(UUID id) {

        PlayResult result = (PlayResult) playResultRepository.getSession()
                .createCriteria(PlayResult.class)
                .add(Restrictions.eq("Id", id))
                .uniqueResult();

        return toScore(result);
    }

    private List<Score> toScore(List<PlayResult> results) {
        List<Score> scores = new ArrayList<Score>();
        for(PlayResult result : results) {
            Score score = toScore(result);

            scores.add(score);
        }

        return scores;
    }

    private Score toScore(PlayResult result) {
        Score score = new Score();
        score.setId(result.getId());
        score.setPlayerOne(result.getPlayerOne().getName());
        score.setPlayerTwo(result.getPlayerTwo().getName());
        score.setScorePlayerOne(result.getScorePlayerOne());
        score.setScorePlayerTwo(result.getScorePlayerTwo());
        score.setPlayTime(result.getPlayTime());
        return score;
    }

}
