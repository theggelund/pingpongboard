package no.dips.pingpongscore.backend.service;

import no.dips.pingpongscore.backend.dto.Result;
import no.dips.pingpongscore.backend.dto.Score;
import no.dips.pingpongscore.backend.repository.ScoreRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.List;

@Path("/results")
@Produces(MediaType.APPLICATION_JSON)
public class ResultService {

    @Autowired
    private ScoreRepository repository;

    @GET
    @Path("/")
    public Response ping() {
        return Response.ok(ResultService.class.getName() + " is mapped to " + ResultService.class.getAnnotation(Path.class).value()).build();
    }

    @GET
    @Path("/lastmonth")
    public Response getLastThirtyDays() {

        HashMap<String, Result> results = new HashMap<String, Result>();
        List<Score> scores = repository.getLastThirtyDaysScore();

        if (scores.size() == 0) {
            return Response.noContent().build();
        }

        for(Score score : scores) {

            Result playerOne = getResultObject(results, score.getPlayerOne());
            Result playerTwo = getResultObject(results, score.getPlayerTwo());

            if (score.getScorePlayerOne() > score.getScorePlayerTwo()) {
                playerOne.incWins();
                playerTwo.incLosses();
            }
            else {
                playerOne.incLosses();
                playerTwo.incWins();
            }

            playerOne.setTotalPoints(playerOne.getTotalPoints() + score.getScorePlayerOne());
            playerTwo.setTotalPoints(playerTwo.getTotalPoints() + score.getScorePlayerTwo());

            playerOne.setTotalNegativePoints(playerOne.getTotalNegativePoints() + score.getScorePlayerTwo());
            playerTwo.setTotalNegativePoints(playerTwo.getTotalNegativePoints() + score.getScorePlayerOne());
        }

        return Response.ok(results.values()).build();
    }

    private Result getResultObject(HashMap<String, Result> results, String playerName) {

        Result player;

        if (results.containsKey(playerName)) {
            player = results.get(playerName);
        }
        else {
            player = new Result();
            player.setPlayerName(playerName);
            results.put(playerName, player);
        }

        return player;
    }


}
