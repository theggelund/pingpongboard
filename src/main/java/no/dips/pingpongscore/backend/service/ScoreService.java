package no.dips.pingpongscore.backend.service;

import no.dips.pingpongscore.backend.dto.Score;
import no.dips.pingpongscore.backend.repository.MissingRequiredFieldException;
import no.dips.pingpongscore.backend.repository.ScoreRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class ScoreService {

    private static final Logger logger = LoggerFactory.getLogger(ScoreService.class);
    public static final int DEFAULT_RESULT_COUNT = 50;

    @Autowired
    private ScoreRepository repository;

    @GET
    @Path("/score/{id}")
    public Response getScore(@QueryParam("id")UUID id) {
        return Response.serverError().build();
    }

    @PUT
    @Path("/score")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response putScore(@Context UriInfo uriInfo, Score score) throws MissingRequiredFieldException {
        if (score == null) {
            logger.error("Unable to deserialize score, or no data sent");
            return Response.noContent().build();
        }

        if (score.getPlayTime() == null) {
            score.setPlayTime(new Date());
        }

        repository.add(score);

        URI url = uriInfo.getAbsolutePathBuilder()
                .path(score.getId().toString())
                .build();

        return Response.created(url).build();
    }

    @GET
    @Path("/scores")
    public Response getScore(@QueryParam("count") Integer resultCount) {
        int maxResultItems = resultCount == null ? DEFAULT_RESULT_COUNT : resultCount.intValue();

        List<Score> score = repository.getOrderByPlayTime(maxResultItems);

        return Response.ok(score).build();
    }
}

