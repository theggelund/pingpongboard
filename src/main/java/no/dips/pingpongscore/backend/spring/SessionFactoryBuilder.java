package no.dips.pingpongscore.backend.spring;

import org.hibernate.SessionFactory;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;

import javax.sql.DataSource;
import java.util.Properties;

public class SessionFactoryBuilder {
    public static SessionFactory build(String packagePath, Properties hibernateProperties, DataSource dataSource) {
        LocalSessionFactoryBuilder builder = new LocalSessionFactoryBuilder(dataSource);

        builder.scanPackages(packagePath)
                .addPackage(packagePath)
                .addProperties(hibernateProperties);

        ServiceRegistry registry = new ServiceRegistryBuilder()
                .applySettings(builder.getProperties())
                .buildServiceRegistry();

        return builder.buildSessionFactory(registry);
    }
}
