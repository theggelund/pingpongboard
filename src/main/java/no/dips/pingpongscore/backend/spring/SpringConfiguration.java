package no.dips.pingpongscore.backend.spring;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import no.dips.pingpongscore.backend.mapping.PlayResult;
import no.dips.pingpongscore.backend.repository.PlayerRepository;
import no.dips.pingpongscore.backend.repository.Repository;
import no.dips.pingpongscore.backend.repository.ScoreRepository;
import no.dips.pingpongscore.backend.service.ResultService;
import no.dips.pingpongscore.backend.service.ScoreService;
import no.dips.pingpongscore.backend.spring.aspect.ServiceExceptionHandlerAspect;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.Properties;

@Configuration
@ComponentScan(value = "no.dips.pingpongscore.backend")
@EnableTransactionManagement
@Import(SpringDatabaseConfiguration.class)
@ImportResource("classpath:spring/spring-config.xml")
public class SpringConfiguration {

    @Bean(name = "scoreService")
    public ScoreService scoreService() {
        return new ScoreService();
    }

    @Bean(name = "resultService")
    public ResultService resultService() {
        return new ResultService();
    }

    @Bean
    public Repository<PlayResult> playResultRepository() {
        return new Repository<PlayResult>();
    }

    @Bean
    public ServiceExceptionHandlerAspect serviceExceptionHandlerAspect() {
        return new ServiceExceptionHandlerAspect();
    }

    @Bean
    public PlayerRepository playerRepository() {
        return new PlayerRepository();
    }

    @Bean
    public ScoreRepository scoreRepository() {
        return new ScoreRepository();
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        Properties properties = new Properties();
        properties.setProperty("jdbc.schema", "update");

        PropertySourcesPlaceholderConfigurer pspc = new PropertySourcesPlaceholderConfigurer();
        pspc.setProperties(properties);

        return pspc;
    }

    @Bean(name = "jsonProvider")
    public JacksonJsonProvider jsonProvider() {
        return new JacksonJsonProvider(
            new ObjectMapper()
                    .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                    .setSerializationInclusion(JsonInclude.Include.NON_NULL)
                    .configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true)
                    .configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, true));

    }

}
