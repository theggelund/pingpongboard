package no.dips.pingpongscore.backend.spring;

import no.dips.pingpongscore.backend.mapping.MappingBase;
import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
public class SpringDatabaseConfiguration {

    @Value("${jdbc.dialect:org.hibernate.dialect.H2Dialect}")
    private String dialect;

    @Value("${jdbc.driver:org.h2.Driver}")
    private String driver;

    @Value("${jdbc.connectionstring:jdbc:h2:~/pingpong}")
    private String connectionString;

    @Value("${jdbc.showsql:false}")
    private Boolean showSql;

    @Value("${jdbc.schema:validate}")
    private String schema;

    @Value("${jdbc.maxconnections:8}")
    private Integer jdbcConnections;

    @Bean
    public DataSource datasource() {
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName(driver);
        ds.setUrl(connectionString);

        if (jdbcConnections != null) {
            ds.setMaxIdle(jdbcConnections);
            ds.setMaxActive(jdbcConnections);
        }

        return ds;
    }

    @Bean
    public PlatformTransactionManager transactionManager() throws IOException {
        return new HibernateTransactionManager(sessionFactory());
    }

    @Bean
    public Properties hibernateProperties() {
        Properties properties = new Properties();

        properties.setProperty("hibernate.connection.driver_class", driver);
        properties.setProperty("hibernate.dialect", dialect);
        properties.setProperty("hibernate.hbm2ddl.auto", schema);
        properties.setProperty("hibernate.show_sql", showSql.toString().toLowerCase());

        return properties;
    }

    @Bean
    public SessionFactory sessionFactory() throws IOException {
        return SessionFactoryBuilder.build(MappingBase.class.getPackage().getName(), hibernateProperties(), datasource());
    }
}

