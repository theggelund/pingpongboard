package no.dips.pingpongscore.backend.spring.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;

@Aspect
public class ServiceExceptionHandlerAspect {

    private static final Logger logger = LoggerFactory.getLogger(ServiceExceptionHandlerAspect.class);

    @Around("execution(* no.dips.pingpongscore.backend.service.*.*(..))")
    public Object exceptionHandler(ProceedingJoinPoint joinPoint) {

        try {
            return joinPoint.proceed();
        }
        catch(Throwable throwable) {
            logger.error("Exception thrown when calling function " + getFunctionName(joinPoint),
                    throwable);

            return Response.serverError().build();
        }
    }

    private String getFunctionName(ProceedingJoinPoint joinPoint) {
        return joinPoint.getSignature().getDeclaringTypeName() + joinPoint.getSignature().getName();
    }
}
