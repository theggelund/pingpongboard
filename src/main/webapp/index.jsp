<html ng-app="PingPongScore">
<head>
    <script src="js/angular.min.js" type="text/javascript"></script>
    <script src="js/angular-resource.min.js" type="text/javascript"></script>
    <script src="js/app.js"></script>
    <script src="js/scorectrl.js"></script>
    <script src="js/resultaggregationctrl.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <meta http-equiv="Access-Control-Allow-Origin" content="*">
</head>
<body>
<h1>PingPong Scoreboard</h1>

<div ng-controller="ScoreCtrl">
    <h3>Last 50 results</h3>

    <form ng-submit="addPlayResult()">
        <input type="text" ng-model="currentScore.playerOne" size="10" placeholder="First player" required>
        <input type="text" ng-model="currentScore.playerTwo" size="10" placeholder="Second player" required>
        <input type="text" ng-model="currentScore.scorePlayerOne" size="7" placeholder="Score first player" required>
        <input type="text" ng-model="currentScore.scorePlayerTwo" size="7" placeholder="Score second player" required>
        <input class="btn-primary" type="submit" value="add">
    </form>

    <ul>
        <li ng-repeat="score in scores">
            {{score.playerOne}} vs {{score.playerTwo}} : {{score.scorePlayerOne}} - {{score.scorePlayerTwo}}
        </li>
    </ul>

</div>

<div ng-controller="ResultAggregationCtrl">
    <h3>Last month</h3>
    <ul>
        <li ng-repeat="result in lastMonthResults">
            {{result.playerName}} - wins: {{result.wins}} losses: {{result.losses}} Points scored: {{result.totalPoints}} Points lost : {{result.totalNegativePoints}}
        </li>
    </ul>
</div>

</body>


</html>
