'use strict';

app.controller('ResultAggregationCtrl', function ResultAggregationCtrl($scope, $location, $http) {

    if ($location.absUrl().indexOf('file://') == 0) {
        $scope.baseUrl = 'http://localhost:8080/no.dips.pingpongscore.backend/';
    }
    else {
        $scope.baseUrl = $location.absUrl();
    }

    $scope.lastMonthResults = [];

    $scope.getLastMonthResults = function() {
        $http.get($scope.baseUrl + 'service/results/lastmonth/')
            .success(function(data) {
                $scope.lastMonthResults = data;
            });
    }

    $scope.getLastMonthResults();

});