'use strict';

app.controller('ScoreCtrl', function ScoreCtrl($scope, $location, $http) {
    $http.defaults.useXDomain = true;

    if ($location.absUrl().indexOf('file://') == 0) {
        $scope.baseUrl = 'http://localhost:8080/no.dips.pingpongscore.backend/';
    }
    else {
        $scope.baseUrl = $location.absUrl();
    }

    $scope.currentScore = null;

    $scope.addPlayResult = function() {
        $http.put($scope.baseUrl + 'service/score/', $scope.currentScore)
            .success(function() {
                $scope.resetCurrentScore();
                $scope.getPreviousScores();

            });
    };

    $scope.resetCurrentScore = function() {
        $scope.currentScore = {
            playerOne: null,
            playerTwo: null,
            scorePlayerOne: null,
            scorePlayerTwo: null,
            playTime: null
        };
    };

    $scope.getPreviousScores = function() {
        $http.get($scope.baseUrl + 'service/scores/')
            .success(function(data) {
                $scope.scores = data;
        });
    }

    $scope.resetCurrentScore();
    $scope.getPreviousScores();
});