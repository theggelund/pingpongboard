package no.dips.pingpongscore.backend;

import no.dips.pingpongscore.backend.mapping.PlayResult;
import no.dips.pingpongscore.backend.mapping.Player;
import no.dips.pingpongscore.backend.spring.SpringDatabaseConfiguration;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;

import java.util.Date;
import java.util.Properties;

@ContextConfiguration( classes =
        {
                MappingTests.class,
                SpringDatabaseConfiguration.class
        })
public class MappingTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        PropertySourcesPlaceholderConfigurer pspc = new PropertySourcesPlaceholderConfigurer();

        Properties properties = new Properties();
        properties.setProperty("jdbc.schema", "create");
        properties.setProperty("jdbc.showsql", "true");
        properties.setProperty("jdbc.connectionstring", "jdbc:h2:mem:");
        pspc.setProperties(properties);

        return pspc;
    }

    @Test
    public void testMapping() throws Exception {
        Player player = createPlayer("dvr");
        Player player2 = createPlayer("lsr");

        PlayResult result = new PlayResult();
        result.setPlayerOne(player);
        result.setPlayerTwo(player2);
        result.setPlayTime(new Date());

        getSession().saveOrUpdate(result);
        getSession().flush();
    }

    private Player createPlayer(String name) {
        Player player = new Player();
        player.setName(name);
        return player;
    }
}
