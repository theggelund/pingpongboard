package repository;

import no.dips.pingpongscore.backend.dto.Score;
import no.dips.pingpongscore.backend.mapping.PlayResult;
import no.dips.pingpongscore.backend.mapping.Player;
import no.dips.pingpongscore.backend.repository.MissingRequiredFieldException;
import no.dips.pingpongscore.backend.repository.PlayerRepository;
import no.dips.pingpongscore.backend.repository.Repository;
import no.dips.pingpongscore.backend.repository.ScoreRepository;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Date;
import java.util.UUID;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ScoreRepositoryTests {

    public static final String playerOneName = "aba";
    public static final String playerTwoName = "bab";

    @InjectMocks
    private ScoreRepository scoreRepository;

    @Mock
    private PlayerRepository playerRepository;

    @Mock
    private Repository<PlayResult> playResultRepository;

    @BeforeMethod(alwaysRun=true)
    public void injectDoubles() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expectedExceptions = MissingRequiredFieldException.class, expectedExceptionsMessageRegExp = "playerOne")
    public void add_whenMissingPlayerOneName_ThrowsMissingRequiredFieldException() throws Exception {
        scoreRepository.add(new Score());
    }

    @Test(expectedExceptions = MissingRequiredFieldException.class, expectedExceptionsMessageRegExp = "playerTwo")
    public void add_whenMissingPlayerTwoName_ThrowsMissingRequiredFieldException() throws Exception {
        Score score = new Score();
        score.setPlayerOne("playerOne");

        scoreRepository.add(score);
    }

    @Test
    public void add_whenPlayerDoesNotExist_playerIsAddedToDb() throws Exception {
        ArgumentCaptor<PlayResult> argument = new ArgumentCaptor<PlayResult>();

        Score score = new Score(UUID.randomUUID(), playerOneName, playerTwoName, 1, 1, new Date());
        scoreRepository.add(score);

        verify(playResultRepository).saveOrUpdate(argument.capture());

        assertThat(argument.getValue()).isNotNull();
        assertThat(argument.getValue().getPlayerOne()).isNotNull();
        assertThat(argument.getValue().getPlayerOne().getName()).isEqualTo(playerOneName);
        assertThat(argument.getValue().getPlayerTwo()).isNotNull();
        assertThat(argument.getValue().getPlayerTwo().getName()).isEqualTo(playerTwoName);
    }

    @Test
    public void add_whenPlayerAlreadyExistsInDb_playerFromDbIsUsed() throws Exception {
        ArgumentCaptor<PlayResult> argument = new ArgumentCaptor<PlayResult>();
        Player aba = createPlayer(UUID.randomUUID(), playerOneName);

        when(playerRepository.getByName(eq(playerOneName)))
                .thenReturn(aba);

        Score score = new Score(UUID.randomUUID(), playerOneName, playerTwoName, 1, 1, new Date());
        scoreRepository.add(score);

        verify(playResultRepository).saveOrUpdate(argument.capture());

        assertThat(argument.getValue().getPlayerOne().getId())
                .isEqualsToByComparingFields(aba.getId());
    }

    private Player createPlayer(UUID id, String name) {
        Player player = new Player();
        player.setId(id);
        player.setName(name);

        return player;
    }
}
